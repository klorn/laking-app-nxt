const x = function () {
    const reg = document.querySelector('#reg-wrap');
    const doc = document.querySelector('#html-select');
    const bodi = document.querySelector('#body-select');

    window.scrollTo({
        top: 0,
        behavior: 'smooth'
    });

    reg.classList.toggle('register');
    reg.classList.toggle('show');
    let times = setTimeout(function() {reg.removeAttribute('style')}, 500); 

    doc.classList.add('html');
    bodi.classList.add('bod');
    
    setTimeout(scrll, 1000)

    function scrll() {
    window.onscroll = function() {scrollFunction()};

    function scrollFunction() {
        if (document.body.scrollTop > 150 || document.documentElement.scrollTop > 150) {
            window.scrollTo({
                top: 150
            })
        }
      }
    };

}



function a() {
    const reg = document.querySelector('#reg-wrap');
    const doc = document.querySelector('#html-select');
    /*const bodi = document.querySelector('#body-select');*/
    const hide = document.querySelector('#change-time');
    const dati = document.querySelector('#slc-date');
    const double = document.querySelector('#slc-double');


    reg.classList.remove('show');
    reg.classList.toggle('register');
    reg.style.opacity = 0;

    doc.classList.remove('html');

    hide.classList.remove('select-date');
    hide.classList.add('hide');
    hide.style.opacity = 0;

    dati.classList.add('select-date');
    dati.classList.remove('hide');

    double.classList.add('select-double');
    double.classList.remove('hide');
    
    function enableScrolling() {
        window.onscroll = function () {};
    }
    enableScrolling();

    

}


function y() {
    const hide = document.querySelector('#change-time');
    const dati = document.querySelector('#slc-date');
    const double = document.querySelector('#slc-double');

    hide.classList.remove('hide');
    hide.classList.add('select-date');
    setTimeout(function() {hide.removeAttribute('style')}, 300);

    dati.classList.add('hide');
    dati.classList.remove('select-date');

    double.classList.add('hide');
    double.classList.remove('select-double');
    
}


function dropMenu() {
    const dati = document.querySelector('.drop-id-date');
    const session = document.querySelector('.drop-id-session');
    const timezone = document.querySelector('.drop-id-timezone');

    dati.classList.toggle('drop-menu');
    dati.classList.toggle('drop-menu-show');

    session.classList.remove('drop-menu-show');
    session.classList.add('drop-menu');

    timezone.classList.remove('drop-menu-show');
    timezone.classList.add('drop-menu');
}

function dropMenu_2() {
    const session = document.querySelector('.drop-id-session');
    const dati = document.querySelector('.drop-id-date');
    const timezone = document.querySelector('.drop-id-timezone');
    

    session.classList.toggle('drop-menu');
    session.classList.toggle('drop-menu-show');

    dati.classList.remove('drop-menu-show');
    dati.classList.add('drop-menu');

    timezone.classList.remove('drop-menu-show');
    timezone.classList.add('drop-menu');
}

function dropMenu_3() {
    const timezone = document.querySelector('.drop-id-timezone');
    const session = document.querySelector('.drop-id-session');
    const dati = document.querySelector('.drop-id-date');

    timezone.classList.toggle('drop-menu');
    timezone.classList.toggle('drop-menu-show');

    session.classList.remove('drop-menu-show');
    session.classList.add('drop-menu');

    dati.classList.remove('drop-menu-show');
    dati.classList.add('drop-menu');
}

function dropMenu_4() {
    const chng = document.querySelector('.drop-id-chng');

    chng.classList.toggle('drop-menu');
    chng.classList.toggle('drop-menu-show');
}

function firstHide() {
    const timezone = document.querySelector('.drop-id-timezone');
    const session = document.querySelector('.drop-id-session');
    const dati = document.querySelector('.drop-id-date');
    const change = document.querySelector('.drop-id-chng');

    timezone.classList.remove('drop-menu-show');
    timezone.classList.add('drop-menu');

    session.classList.remove('drop-menu-show');
    session.classList.add('drop-menu');

    dati.classList.remove('drop-menu-show');
    dati.classList.add('drop-menu');

    change.classList.remove('drop-menu-show');
    change.classList.add('drop-menu');
}

function emailHide() {
    const timezone = document.querySelector('.drop-id-timezone');
    const session = document.querySelector('.drop-id-session');
    const dati = document.querySelector('.drop-id-date');
    const change = document.querySelector('.drop-id-chng');

    timezone.classList.remove('drop-menu-show');
    timezone.classList.add('drop-menu');

    session.classList.remove('drop-menu-show');
    session.classList.add('drop-menu');

    dati.classList.remove('drop-menu-show');
    dati.classList.add('drop-menu');

    change.classList.remove('drop-menu-show');
    change.classList.add('drop-menu');
}


function hide() {
    const dati = document.querySelector('.drop-id-date');
    const session = document.querySelector('.drop-id-session');
    const timezone = document.querySelector('.drop-id-timezone');
    const chng = document.querySelector('.drop-id-chng');

    dati.classList.remove('drop-menu-show');
    dati.classList.add('drop-menu');

    session.classList.remove('drop-menu-show');
    session.classList.add('drop-menu');

    timezone.classList.remove('drop-menu-show');
    timezone.classList.add('drop-menu');

    chng.classList.remove('drop-menu-show');
    chng.classList.add('drop-menu');
}